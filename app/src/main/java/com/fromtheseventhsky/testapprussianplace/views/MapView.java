package com.fromtheseventhsky.testapprussianplace.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FromTheSeventhSky in 2016.
 */

public class MapView extends View {

    private final int mTileSize = 128;
    private final int mCanvasSize = 12800;


    private int mDisplayWidth = 0;
    private int mDisplayHeight = 0;


    private boolean mFirstOpened;

    private List<MyTarget> mTargets;

    private Context mContext;

    private ArrayList<Bitmap> mBitmapInMemory = null;

    private Paint mPaint = null;

    private int mTileCountHorizontal = 0;
    private int mTileCountVertical = 0;

    private int mTotalVisibleTileCount = 0;
    private int mTotalTileCount = 0;

    private int mCurrentVisibleTopX;
    private int mCurrentVisibleTopY;

    private final GestureDetector detector;


    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mPaint = new Paint();
        mFirstOpened = false;
        detector = new GestureDetector(context, new MyGestureListener());
        mBitmapInMemory = new ArrayList<>();
        mTargets = new ArrayList<>();
    }


    //вычисляем размеры, сколько тайлов помещается на экране, инициализируем и заполняем матрицы
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mDisplayWidth = MeasureSpec.getSize(widthMeasureSpec);
        mDisplayHeight = MeasureSpec.getSize(heightMeasureSpec);


        mTileCountHorizontal = mDisplayWidth / 128 + 3;
        mTileCountVertical = mDisplayHeight / 128 + 3;

        mTotalTileCount = mTileCountHorizontal * mTileCountVertical;

        if (mBitmapInMemory.size() != mTotalTileCount) {
            mBitmapInMemory.clear();
            for (int i = 0; i < mTotalTileCount; i++) {
                mBitmapInMemory.add(null);
            }
            mTargets.clear();
            for (int i = 0; i < mTotalTileCount; i++) {
                mTargets.add(new MyTarget(i));
            }
        }
        mCurrentVisibleTopX = 0;
        mCurrentVisibleTopY = 0;
        setMeasuredDimension(mDisplayWidth | MeasureSpec.EXACTLY, mDisplayHeight | MeasureSpec.EXACTLY);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        //что в оперативке - то добавляем, что нет - подгружаем
        for (int i = 0; i < mBitmapInMemory.size(); i++) {
            if (mBitmapInMemory.get(i) != null && !mBitmapInMemory.get(i).isRecycled()) {
                canvas.drawBitmap(
                        mBitmapInMemory.get(i),
                        i % mTileCountHorizontal * 128 - 128 + mCurrentVisibleTopX * 128,
                        (i / mTileCountHorizontal) * 128 - 128 + mCurrentVisibleTopY * 128,
                        mPaint);
            } else {
                loadIntoTile(i);
            }
        }
    }

    private void loadIntoTile(final int number) {
        mTargets.get(number).setPosition(number);
        String link = "";

        //по идее, тут нужно в зависимости от текущих координат верхнего левого угла подбирать соответствующую
        //ссылку для подгрузки, но сейчас для наглядности берется просто переданный номер
        //потому при прокрутке творится безумие, хотя вполне логичное безумие)
        if (number % 3 == 0) {
            link = "http://www.omnionline.com.au/wp-content/uploads/2014/03/OMNI_ONLINE_new_Google_-ico.png";
        } else {
            link = "http://a4.mzstatic.com/us/r30/Purple41/v4/ee/d6/44/eed6447a-34d2-d3cd-2800-7f845d9b08bf/icon175x175.png";
        }
        Picasso.with(mContext)
                .load(link)
                .resize(128, 128)
                .into(mTargets.get(number));

    }


    public boolean onTouchEvent(MotionEvent event) {
        detector.onTouchEvent(event);
        return true;
    }

    //таргет для пикассо, внутри номер ячейки ArrayList'а, в которую помещать полученный битмап
    public class MyTarget implements Target {

        private int mPosition;

        MyTarget(int position) {
            super();
            mPosition = position;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mBitmapInMemory.set(mPosition, bitmap);
            invalidate();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

        public int getPosition() {
            return mPosition;
        }

        public void setPosition(int mPosition) {
            this.mPosition = mPosition;
        }
    }


    //слушатель для скролла, чтоб удалять неиспользуемые строки и столбцы и добавлять на их место
    //пустые, нуждающиеся в заполнении
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        int tmpX = mCurrentVisibleTopX;
        int tmpY = mCurrentVisibleTopY;
        mCurrentVisibleTopX = l / 128;
        mCurrentVisibleTopY = t / 128;
        //если скроллим влево, добавляем слева пустой столбец, справа удаляем
        if (tmpX > mCurrentVisibleTopX) {
            for (int i = 0; i < mTileCountVertical; i++) {
                mBitmapInMemory.add(i * mTileCountHorizontal, null);
                mBitmapInMemory.remove((i + 1) * mTileCountHorizontal);
            }
        }
        //если скроллим вправо, добавляем пустой столбец справа, слева удаляем
        if (tmpX < mCurrentVisibleTopX) {
            for (int i = 0; i < mTileCountVertical; i++) {
                mBitmapInMemory.add((i + 1) * mTileCountHorizontal, null);
                mBitmapInMemory.remove(i * mTileCountHorizontal);
            }
        }
        //если скроллим вниз, то добавляем сверху пустую строку, самую нижнюю прокрученную строку удаляем
        if (tmpY > mCurrentVisibleTopY) {
            for (int i = 0; i < mTileCountHorizontal; i++) {
                mBitmapInMemory.add(i, null);
            }
            for (int i = mTotalTileCount - mTileCountHorizontal; i < mTotalTileCount; i++) {
                mBitmapInMemory.remove(i);
            }
        }
        //если скроллим вверх, то добавляем снизу пустую строку, самую верхнюю прокрученную строку удаляем
        if (tmpY < mCurrentVisibleTopY) {
            for (int i = 0; i < mTileCountHorizontal; i++) {
                mBitmapInMemory.remove(i);
            }
            for (int i = mTotalTileCount - mTileCountHorizontal; i < mTotalTileCount; i++) {
                mBitmapInMemory.add(i, null);
            }
        }

    }

    //слушатель жестов для запрета выхода за рамки 100х100
    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            //границы оси Ох
            if (getScrollX() + distanceX < mCanvasSize - mDisplayWidth && getScrollX() + distanceX > 0) {
                scrollBy((int) distanceX, 0);
            }
            //границы оси Оу
            if (getScrollY() + distanceY < mCanvasSize - mDisplayHeight && getScrollY() + distanceY > 0) {
                scrollBy(0, (int) distanceY);
            }
            return true;
        }
    }
}
